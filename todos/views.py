from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.

def show_list(request):
    list = TodoList.objects.all()
    context = {
        "show_list": list
    }
    return render(request, "todos/list.html", context)

def show_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "todolist": list
    }
    return render(request, "todos/detail.html", context)

def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save(False)
            todolist.save()
            return redirect("todo_list_detail", todolist.id)
    else:
        form = TodoListForm()
        context = {
            "form": form,
        }
        return render(request, "todos/create.html", context)

def edit_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=list)
        context = {
            "todolist": list,
            "form": form,
        }
        return render(request, "todos/edit.html", context)

def delete_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    else:
        form = TodoListForm(instance=list)
        context = {
            "todolist": list,
            "form": form,
        }
        return render(request, "todos/delete.html", context)

def create_item(request):

    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save(False)
            todoitem.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm()
        context = {
            "form": form,
        }
        return render(request, "todos/itemscreate.html", context)

def edit_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", item.list.id)
    else:
        form = TodoItemForm(instance=item)
        context = {
            "todoitem": item,
            "form": form,
        }
        return render(request, "todos/itemsedit.html", context)
